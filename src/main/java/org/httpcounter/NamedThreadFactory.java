package org.httpcounter;

import java.util.concurrent.ThreadFactory;

public class NamedThreadFactory implements ThreadFactory {
    private final String name;
    private int count;

    public NamedThreadFactory(String name) {
        this.name = name;
    }

    @Override
    public Thread newThread(Runnable r) {
        count++;
        final Thread thread = new Thread(r);
        thread.setName(name + "-thread-" + count);
        return thread;
    }
}
