package org.httpcounter;

import org.httpcounter.config.Configs;
import org.httpcounter.database.DatabaseFactory;
import org.httpcounter.http.HttpService;
import org.httpcounter.http.UriProcessingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HttpCounter {
    private static final HttpService httpService = new HttpService();
    private static final UriProcessingService uriProcessingService = new UriProcessingService();
    private static final Logger log = LoggerFactory.getLogger(HttpCounter.class);

    public static void main(String[] args) {
        try {
            log.info("HttpCounter starting");
            Configs.load();
            DatabaseFactory.init();
            Runtime.getRuntime().addShutdownHook(new ShutdownHook());
            httpService.start(uriProcessingService);
            uriProcessingService.start();
            log.info("HttpCounter started");
        } catch (IOException e) {
            log.error("Can't start httpServer : ", e);
            System.exit(1);
        } catch (Exception e) {
            log.error("Unexpected error : ", e);
            System.exit(-1);
        }
    }

    static void shutdown() {
        log.info("Shutdown hook started.");
        httpService.shutdown();
        uriProcessingService.shutdown();
        DatabaseFactory.shutdown();
        log.info("Program shutting down.");
    }
}
