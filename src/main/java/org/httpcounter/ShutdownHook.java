package org.httpcounter;

class ShutdownHook extends Thread {
    @Override
    public void run() {
        HttpCounter.shutdown();
    }
}
