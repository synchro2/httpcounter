package org.httpcounter.config;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:config/database.properties"})
public interface DatabaseConfig extends Config {

    @Key("database.url")
    @DefaultValue("jdbc:clickhouse://127.0.0.1:8123/httpcounter")
    String dbUrl();

    @Key("database.user")
    String dbUser();

    @Key("database.password")
    String dbPassword();
}

