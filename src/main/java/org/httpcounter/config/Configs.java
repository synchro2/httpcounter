package org.httpcounter.config;

import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configs {
    private static final Logger log = LoggerFactory.getLogger(Configs.class);

    public static DatabaseConfig database;

    public static void load() {
        database = ConfigFactory.create(DatabaseConfig.class);
        log.info("Configs loaded");
    }
}
