package org.httpcounter.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.httpcounter.NamedThreadFactory;
import org.httpcounter.config.Configs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseFactory {
    private static HikariDataSource dataSource;

    private static final Logger log = LoggerFactory.getLogger(DatabaseFactory.class);

    private DatabaseFactory() {
    }

    public static void init() {
        HikariConfig config = new HikariConfig();
        config.setMaximumPoolSize(Runtime.getRuntime().availableProcessors());
        config.setThreadFactory(new NamedThreadFactory("HikariCP"));
        config.setConnectionTimeout(5000);
        config.setDriverClassName("ru.yandex.clickhouse.ClickHouseDriver");
        config.setUsername(Configs.database.dbUser());
        config.setPassword(Configs.database.dbPassword());
        config.setJdbcUrl(Configs.database.dbUrl());

        dataSource = new HikariDataSource(config);
        log.info("DatabaseFactory started");
    }

    static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public static void shutdown() {
        dataSource.close();
    }

    static HikariDataSource getDataSource() {
        return dataSource;
    }
}