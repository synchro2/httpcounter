package org.httpcounter.database;

import org.httpcounter.HttpCounter;
import org.httpcounter.http.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class RequestDAO {
    private static final String INSERT_ENTRY = "INSERT INTO request (ip, referrer, path) VALUES(?,?,?)";
    private static final int BATCH_LIMIT = 5000;

    private static final Logger log = LoggerFactory.getLogger(HttpCounter.class);

    public static void insertRequest(List<Query> queries) {
        int batchSize = 0;

        try (Connection con = DatabaseFactory.getConnection();
             PreparedStatement ps = con.prepareStatement(INSERT_ENTRY)) {
            for (Query query : queries) {
                ps.setString(1, query.getIp());
                ps.setString(2, query.getReferrer());
                ps.setString(3, query.getPath());
                ps.addBatch();
                batchSize++;
                if (batchSize >= BATCH_LIMIT) {
                    ps.executeBatch();
                    batchSize = 0;
                }
            }
            ps.executeBatch();
            log.info("Inserted {} requests into db", queries.size());
        } catch (SQLException e) {
            log.error("Error on insert info in DB, information lost", e);
        }
    }
}