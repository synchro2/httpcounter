package org.httpcounter.http;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.util.ReferenceCountUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.netty.handler.codec.http.HttpResponseStatus.NO_CONTENT;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

@ChannelHandler.Sharable
class HttpHandlerImpl extends ChannelDuplexHandler {

    private static final DefaultFullHttpResponse DEFAULT_FULL_HTTP_RESPONSE = new DefaultFullHttpResponse(HTTP_1_1, NO_CONTENT);
    private final UriProcessingService uriProcessingService;

    HttpHandlerImpl(UriProcessingService uriProcessingService) {
        this.uriProcessingService = uriProcessingService;
    }

    private static final Logger log = LoggerFactory.getLogger(HttpHandlerImpl.class);

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
        log.error(cause.getMessage(), cause);
    }

    @Override
    public void channelRead(ChannelHandlerContext context, Object message) {
        try {
            if (message instanceof FullHttpRequest) {
                onRequest(context, (FullHttpRequest) message);
            }
        } finally {
            ReferenceCountUtil.release(message);
        }
    }

    private void onRequest(ChannelHandlerContext context, FullHttpRequest request) {
        uriProcessingService.addUri(request.uri());
        sendNoContent(context, HttpUtil.isKeepAlive(request));
    }

    private void sendNoContent(ChannelHandlerContext context, boolean keepAlive) {
        if (!context.isRemoved() && context.channel().isWritable()) {
            context.channel().eventLoop().execute(() -> {
                if (keepAlive) {
                    context.writeAndFlush(DEFAULT_FULL_HTTP_RESPONSE.retainedDuplicate(), context.voidPromise());
                } else {
                    context.writeAndFlush(DEFAULT_FULL_HTTP_RESPONSE.retainedDuplicate()).addListener(ChannelFutureListener.CLOSE);
                }
            });
        }
    }
}
