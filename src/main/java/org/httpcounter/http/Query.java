package org.httpcounter.http;

import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.util.NetUtil;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Query {
    private final String path;
    private final String ip;
    private final String referrer;

    private static final int URI_LENGTH_LIMIT = 2000;

    Query(String query) {
        QueryStringDecoder decoder = new QueryStringDecoder(query);
        final Map<String, List<String>> parameters = decoder.parameters();
        final List<String> paths = parameters.get("path");
        final List<String> ips = parameters.get("ip");
        final List<String> referrers = parameters.get("referrer");

        this.path = Objects.isNull(paths) ? "" : paths.get(0);
        this.ip = Objects.isNull(ips) ? "" : ips.get(0);
        this.referrer = Objects.isNull(referrers) ? "" : referrers.get(0);
    }

    boolean isValid() {
        return path.length() <= URI_LENGTH_LIMIT && referrer.length() <= URI_LENGTH_LIMIT
                && (NetUtil.isValidIpV4Address(ip) || NetUtil.isValidIpV6Address(ip));
    }

    public String getPath() {
        return path;
    }

    public String getIp() {
        return ip;
    }

    public String getReferrer() {
        return referrer;
    }
}
