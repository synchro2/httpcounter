package org.httpcounter.http;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class HttpService {
    private static final int PORT = 80;

    private ServerBootstrap bootstrap;
    private ChannelFuture channelFuture;
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;

    private static final Logger log = LoggerFactory.getLogger(HttpService.class);

    public void start(UriProcessingService uriProcessingService) throws Exception {
        final boolean useEpoll = Epoll.isAvailable();
        bossGroup = useEpoll ? new EpollEventLoopGroup(1) : new NioEventLoopGroup(1);
        workerGroup = useEpoll ? new EpollEventLoopGroup() : new NioEventLoopGroup();

        bootstrap = new ServerBootstrap().group(bossGroup, workerGroup)
                .channel(useEpoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
                .childHandler(new ChannelInitializerImpl(uriProcessingService).invoke())
                .option(ChannelOption.SO_BACKLOG, 1024)
                .option(ChannelOption.SO_REUSEADDR, true)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.SO_REUSEADDR, true)
                .childOption(ChannelOption.TCP_NODELAY, true);
        if (useEpoll) {
            bootstrap
                    .childOption(EpollChannelOption.TCP_CORK, false)
                    .childOption(EpollChannelOption.TCP_FASTOPEN_CONNECT, true);
        }
        channelFuture = bootstrap
                .bind(PORT)
                .sync();

        log.info("HttpService started, using useEpoll: {}", useEpoll);
    }

    public void shutdown() {
        channelFuture.cancel(false);
        channelFuture.awaitUninterruptibly(1, TimeUnit.MINUTES);
        final Future<?> workerGroupFuture = workerGroup.shutdownGracefully(0, 60, TimeUnit.SECONDS);
        workerGroupFuture.awaitUninterruptibly();
        final Future<?> bossGroupFuture = bossGroup.shutdownGracefully(0, 60, TimeUnit.SECONDS);
        bossGroupFuture.awaitUninterruptibly();
    }

    ServerBootstrap getBootstrap() {
        return bootstrap;
    }

    private static class ChannelInitializerImpl {
        private final UriProcessingService uriProcessingService;

        private ChannelInitializerImpl(UriProcessingService uriProcessingService) {
            this.uriProcessingService = uriProcessingService;
        }

        private ChannelInitializer<Channel> invoke() {
            return new ChannelInitializer<>() {
                @Override
                public void initChannel(Channel channel) {
                    ChannelPipeline pipeline = channel.pipeline();
                    pipeline.addLast(new HttpServerCodec());
                    pipeline.addLast(new HttpObjectAggregator(1024));
                    pipeline.addLast(new HttpHandlerImpl(uriProcessingService));
                }
            };
        }
    }
}
