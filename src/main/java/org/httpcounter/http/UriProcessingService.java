package org.httpcounter.http;

import org.httpcounter.NamedThreadFactory;
import org.httpcounter.database.RequestDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;

public class UriProcessingService {
    private final Queue<String> uriQueue = new ConcurrentLinkedQueue<>();
    private final ScheduledExecutorService scheduledService = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("ScheduleDB"));
    private final ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), new NamedThreadFactory("InsertDB"));
    private ScheduledFuture<?> scheduledFuture;

    private static final Logger log = LoggerFactory.getLogger(UriProcessingService.class);

    public void start() {
        scheduledFuture = scheduledService.scheduleAtFixedRate(this::execute, 150, 150, TimeUnit.MILLISECONDS);
        log.info("UriProcessingService started");
    }

    public void shutdown() {
        scheduledService.shutdown();
        service.shutdown();
        try {
            scheduledService.awaitTermination(10, TimeUnit.SECONDS);
            service.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            log.error("Error while stopping UriProcessingService: ", e);
        }
    }

    private void execute() {
        List<Query> queries = new ArrayList<>();
        String uri;
        while ((uri = uriQueue.poll()) != null) {
            Query query = new Query(uri);
            if (query.isValid()) {
                queries.add(query);
            }
        }
        if (!queries.isEmpty()) {
            service.execute(() -> RequestDAO.insertRequest(queries));
        }
    }

    void addUri(String requestURI) {
        if (!uriQueue.offer(requestURI)) {
            log.error("uriQueue is full, request lost");
        }
    }

    Queue<String> getUriQueue() {
        return uriQueue;
    }

    ScheduledExecutorService getScheduledService() {
        return scheduledService;
    }

    ExecutorService getService() {
        return service;
    }

    ScheduledFuture<?> getScheduledFuture() {
        return scheduledFuture;
    }
}