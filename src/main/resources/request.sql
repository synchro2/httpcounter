CREATE TABLE httpcounter.request (
  ip String,
  referrer String,
  path String,
  eventDate Date DEFAULT today(),
  eventTime DateTime DEFAULT now()
) ENGINE = MergeTree(eventDate, (eventTime, path), 8192)