package org.httpcounter.http;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UriProcessingServiceTest {

    @Test
    void start() {
        UriProcessingService uriProcessingService = new UriProcessingService();
        uriProcessingService.start();
        assertNotNull(uriProcessingService.getScheduledFuture());
        uriProcessingService.shutdown();
    }

    @Test
    void shutdown() {
        UriProcessingService uriProcessingService = new UriProcessingService();
        uriProcessingService.start();
        uriProcessingService.shutdown();
        assertTrue(uriProcessingService.getScheduledService().isShutdown());
        assertTrue(uriProcessingService.getScheduledService().isTerminated());
        assertTrue(uriProcessingService.getService().isShutdown());
        assertTrue(uriProcessingService.getService().isTerminated());
    }

    @Test
    void addUri() {
        UriProcessingService uriProcessingService = new UriProcessingService();
        uriProcessingService.addUri("test");
        assertFalse(uriProcessingService.getUriQueue().isEmpty());
        uriProcessingService.getUriQueue().clear();
    }
}