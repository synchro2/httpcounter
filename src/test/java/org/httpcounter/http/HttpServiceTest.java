package org.httpcounter.http;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HttpServiceTest {

    @Test
    void start() {
        HttpService httpService = new HttpService();
        assertDoesNotThrow(() -> httpService.start(new UriProcessingService()));
        assertNotNull(httpService.getBootstrap());
    }

    @Test
    void shutdown() {
        HttpService httpService = new HttpService();
        final UriProcessingService uriProcessingService = new UriProcessingService();
        assertDoesNotThrow(() -> httpService.start(uriProcessingService));
        assertDoesNotThrow(httpService::shutdown);
        assertTrue(httpService.getBootstrap().config().group().isTerminated());
    }
}