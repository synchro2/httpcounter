package org.httpcounter.http;

import com.google.common.base.Strings;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QueryTest {

    @Test
    void isValid() {
        final String validPath = "/?path=www.test.org/test&ip=1.1.1.1&referrer=www.test.org/referrer";
        final String validPathIpV6 = "/?path=www.test.org/test&ip=FE80:0000:0000:0000:0202:B3FF:FE1E:8329&referrer=www.test.org/referrer";
        final String notIp = "/?path=www.test.org/test&ip=testIp&referrer=www.test.org/referrer";
        final String pathTooLong = "/?path=" + Strings.repeat("", 2001) + "&ip=testIp&referrer=www.test.org/referrer";
        final String referrerTooLong = "/?path=www.test.org/test&ip=testIp&referrer=" + Strings.repeat("", 2001);

        Query query = new Query(validPath);
        assertTrue(query.isValid());

        query = new Query(validPathIpV6);
        assertTrue(query.isValid());

        query = new Query(notIp);
        assertFalse(query.isValid());

        query = new Query(pathTooLong);
        assertFalse(query.isValid());

        query = new Query(referrerTooLong);
        assertFalse(query.isValid());
    }

    @Test
    void getPath() {
        final String path = "/?path=www.test.org/test";
        Query query = new Query(path);
        assertEquals(path.substring(7), query.getPath());
    }

    @Test
    void getIp() {
        final String ip = "/?ip=1.1.1.1";
        Query query = new Query(ip);
        assertEquals(ip.substring(5), query.getIp());
    }

    @Test
    void getReferrer() {
        final String referrer = "/?referrer=www.test.org/referrer";
        Query query = new Query(referrer);
        assertEquals(referrer.substring(11), query.getReferrer());
    }
}