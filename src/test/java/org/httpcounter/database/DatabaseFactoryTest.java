package org.httpcounter.database;

import org.httpcounter.config.Configs;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DatabaseFactoryTest {
    @BeforeEach
    void setUp() {
        Configs.load();
    }

    @Test
    void init() {
        DatabaseFactory.init();
        assertNotNull(DatabaseFactory.getDataSource());
    }

    @Test
    void getConnection() {
        DatabaseFactory.init();
        assertDoesNotThrow(DatabaseFactory::getConnection);
    }

    @Test
    void shutdown() {
        DatabaseFactory.shutdown();
        assertTrue(DatabaseFactory.getDataSource().isClosed());
    }
}