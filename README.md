Http Counter
====================
High performance java app for tracking basic user activity on web site.

Requirements:
-------------
1. JDK or JRE 10 or higher
2. ClickHouse 1.1
